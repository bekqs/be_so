# BE_SO
### Profile page template

---

Run scripts:

* npm start
* Gulp

Used technologies:

* Webpack
* Babel
* Gulp
* Sass

Link to:
**[Demo page](https://bekqs.bitbucket.io/)**.